<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cat_id');
            $table->mediumText('seo_link')->nullable();
            $table->foreign('cat_id')->references('id')->on('categories')->onDelete('cascade');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('title');
            $table->string('img')->nullable();
            $table->string('img2')->nullable();
            $table->text('description');
            $table->boolean('draft')->default(0);
            $table->boolean('category_head')->default(0);
            $table->boolean('slider_1_welcome')->default(0);
            $table->boolean('slider_2_welcome')->default(0);
            $table->boolean('slider_3_welcome')->default(0);
            $table->boolean('welcome_bottom')->default(0);
            $table->boolean('aprove')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
