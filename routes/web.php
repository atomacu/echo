<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('command', function () {
    \Artisan::call('migrate');
    dd("Done");
});


Route::group(['middleware' => ['role']], function(){

    Route::get('/', 'WelcomeController@index')->name('welcome.index');
    Route::get('/ads.txt', 'WelcomeController@adsTxt')->name('welcome.adsTxt');
    
    Route::resource('welcome', 'WelcomeController');
    Auth::routes();
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('admin', 'Admin\AdminController');
    Route::resource('image/upload', 'Admin\ImagesController');
    Route::resource('category', 'Admin\CategoryController');
    Route::put('category-name/{category}', 'Admin\CategoryController@updateName')->name('category.name.update'); 
   
    Route::put('category/setOnWelcome/{id}', 'Admin\CategoryController@setOnWelcome')->name('category.setOnWelcome'); 
    
    Route::resource('post', 'Admin\PostController')->except([
        'create'
    ]); 
    Route::get('/post/create/{id}', 'Admin\PostController@create')->name('post.create'); 
    Route::put('/post/setOnCategory/{id}', 'Admin\PostController@setOnCategory')->name('post.setOnCategory'); 
    Route::resource('draft', 'Admin\DraftController');
    Route::resource('aprove', 'Admin\AproveController');
    Route::get('/aprove/showRejected/{id}', 'Admin\AproveController@showRejected')->name('aprove.showRejected'); 
    Route::resource('welcomeset', 'Admin\WelcomeSettingsController');
    Route::get('/welcomeset/firstSlidetGet/{id}', 'Admin\WelcomeSettingsController@firstSlidetGet')->name('welcomeset.firstSlidetGet'); 

    Route::PUT('welcomeset/updateFirstSlider/{id}', 'Admin\WelcomeSettingsController@updateFirstSlider')->name('welcomeset.updateFirstSlider'); 
    
    
    
    Route::resource('ucategory', 'User\CategoryController');
    Route::resource('articles', 'User\PostController');
    Route::resource('uposts', 'User\PostController');

    Route::resource('redactor', 'Redactor\RedactorController');






    
    
});


