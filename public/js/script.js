function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        $('#image-file').removeClass('d-none');
        $('.close-img-btn').removeClass('d-none');
        $('.img-div').removeClass('d-none');
        reader.onload = function (e) {
            $('#image-file')
                .attr('src', e.target.result)
                .width('350px').height('350px');

            var image = $('#image-file');
            
            image.cropper({
                aspectRatio: 1 / 1,
                cropBoxResizable: false,
                minCropBoxWidth: 200,
                minCropBoxHeight: 200,
                crop(event) {
                    originalData = image.cropper("getCroppedCanvas");
                    $('#add-post-form, #update-post-form').data('img1', originalData.toDataURL());
                    
                },
            });
            image.cropper('replace', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }



}

function readURL2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        $('#image-file2').removeClass('d-none');
        $('.close-img-btn2').removeClass('d-none');
        $('.img-div').removeClass('d-none');
        reader.onload = function (e) {
            $('#image-file2')
                .attr('src', e.target.result)
                .width('100%');
            var image2 = $('#image-file2');
            image2.cropper({
                aspectRatio: 3 / 1,
                minCropBoxWidth: 200,
                minCropBoxHeight: 200,
                cropBoxResizable: false,
              
                crop(event) {
                    originalData = image2.cropper("getCroppedCanvas");
                    $('#add-post-form, #update-post-form').data('img2', originalData.toDataURL());
                },
            });
            image2.cropper('replace', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}





$(document).ready(function () {


    // ascunde si afiseaza inputul
    $('#plusButton').click(function () {
        let inputForm = $('#input-category');
        if ($('#input-category').data('toggle') == 0) {
            $(inputForm).removeClass('d-none');
            $(inputForm).data('toggle', 1);
            $(this).html('<i class="fas fa-times"></i>');
        } else {
            $(inputForm).addClass('d-none');
            $(inputForm).data('toggle', 0);
            $(this).html('<i class="fas fa-plus"></i>');
        }
    });


    $("#add-category-btn").click(function () {
        let url = $(this).data('url');
        let category = $('#category-input').val();
        let inputForm = $('#input-category');
        if (category == "") {
            $('.alert').remove();
            $(this).parent().parent().append('<h6 class="alert alert-danger mt-3 mx-auto text-weight-bold"> Completeaza! </h6>')
        } else {
            axios.post(url, {
                    category: category
                })
                .then(function (response) {
                    $('.categories').append(response.data);
                    $('#category-input').val("");
                    $(inputForm).addClass('d-none');
                    $(inputForm).data('toggle', 0);

                })
                .catch(function (error) {
                    console.log(error);
                });
        }

    });

    $(document).on('click', '.up-btn', function () {
        let index = $(this).data('index');
        let url = $(this).data('url');
        axios.put(url, {
                index: index,
                data: 'up'
            })
            .then(function (response) {
                if (response.data != 0)
                    $('.div-table').html(response.data);

            })
            .catch(function (error) {
                console.log(error);
            });

    });

    $(document).on('click', '.down-btn', function () {
        let index = $(this).data('index');
        let url = $(this).data('url');
        axios.put(url, {
                index: index,
                data: 'down'
            })
            .then(function (response) {
                if (response.data != 0)
                    $('.div-table').html(response.data);

            })
            .catch(function (error) {
                console.log(error);
            });
    });

    $(document).on('click', '.btn-edit-category-name', function () {

        let id = $(this).data('id');
        let url = $(this).data('url');
        console.log(id)
        let name = $('#input-edit-category-' + id).val();
        console.log(name);
        if (name != "") {
            $('#edit-category-modal-' + id).modal('hide');
            axios.put(url, {
                    name: name,
                })
                .then(function (response) {
                    console.log(response.data);
                    $('.div-category-name-' + id).html(response.data);
                    $('.nav-category-name-' + id).html(name);
                })
                .catch(function (error) {
                    console.log(error);
                });
        } else
            alert('Completeaza cimpul');
    });

    $(document).on('click', '.btn-delete-category ', function () {
        let url = $(this).data('url');
        let id = $(this).data('id');
        axios.delete(url)
            .then(function (response) {
                // console.log(response.data);
                $('.div-table').html(response.data);
                $('#nav-category-' + id).remove();
            })
            .catch(function (error) {
                console.log(error);
            });
    });

    $(document).on('click', '.close-img-btn', function () {
        $('.img-div').addClass('d-none');
        $('.close-img-btn').addClass('d-none');
        $('input.main-image').val('');

    });

    $(document).on('click', '.close-img-btn2', function () {
        $('.img-div2').addClass('d-none');
        $('.close-img-btn2').addClass('d-none');
        $('input.main-image2').val('');

    });
    // $( 'textarea.editor' ).ckeditor();

    $('#add-post-form').submit(function (e) {
        e.preventDefault();
        let url = $(this).data('url');
        let data = new FormData(this);
        let title = $('textarea#post-title').val();
        let description = $('textarea#editor').val();
        let catID = $(this).data('id');
        let draft = $(this).data('draft');
        let img1 = $(this).data('img1');
        let img2 = $(this).data('img2')
        data.append('title', title);
        data.append('description', description);
        data.append('catID', catID);
        data.append('draft', draft);
        data.append('img1', img1);
        data.append('img2', img2);
       
        axios.post(url, data, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then(function (response) {
                window.location.replace(response.data);
            });
    });

    $('#add-post-draft').click(function () {
        $('#add-post-form').data('draft', 1);
    });

    $('#add-post-btn-add').click(function () {
        $('#add-post-form').data('draft', 0);
    });

    $('#update-post-form').submit(function (e) {
        e.preventDefault();
        $('.alert').remove();
        let url = $(this).data('url');
        let data = new FormData(this);
        let title = $('textarea#post-title').val();
        let description = $('textarea#editor').val();
        let img1 = $(this).data('img1');
        let img2 = $(this).data('img2')
        let check = 0;
        if (title == "") {
            $('textarea#post-title').parent().append('<p class="alert m-0 p-0" style="color:brown; font-weight:bold; ">Spațul titlului trebuie sa fie completat.</p>')
            $('html, body').animate({
                scrollTop: $("textarea#post-title").offset().top
            }, 700);
        } else {
            check++;
        }

        if (description == "") {
            $('textarea#ckeditor-text').parent().append('<p class="alert m-0 p-0" style="color:brown; font-weight:bold; ">Spațul articolului trebuie sa fie completat.</p>')

        } else {
            check++;
        }

        if (check == 2) {
            data.append('img1', img1);
            data.append('img2', img2);
            data.append('title', title);
            data.append('description', description);

            axios.post(url, data, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    }
                })
                .then(function (response) {
                    window.location.replace(response.data);
                });


        }

    });


    $('.switch-post').change(function () {
        var id = $(this).data('id');
        var url = $(this).data('url');

        $('.switch-post').each(function () {
            if ($(this).data('id') != id) {
                $(this).prop('checked', false);
            }
        });

        axios.put(url)
            .then(function (response) {

            })
            .catch(function (error) {

            });
    });


    $('.switch-category-welcome').change(function () {
        var id = $(this).data('id');
        var url = $(this).data('url');
        console.log(this);
        $('.switch-category-welcome').each(function () {
            if ($(this).data('id') != id) {
                $(this).prop('checked', false);
            }
        });
        axios.put(url)
            .then(function (response) {

            })
            .catch(function (error) {

            });
    });


    $('#delete-btn-post').click(function () {
        var url = $(this).data('url');
        console.log(url);
        axios.delete(url)
            .then(function (response) {
                window.location.replace(response.data);
            })
            .catch(function (error) {
                console.log(error);
            });
    });

    $('#post-draft-btn').click(function () {
        var url = $(this).data('url');
        console.log(url);

        $.ajax({
            type: 'PUT',
            cache: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
            },
            dataType: "text",
            success: function (data) {
                window.location.replace(data);
            },
            error: function (data) {},
        });
    });

    $('.aprove-post-btn').click(function () {
        var url = $(this).data("url");
        var aproveStatus = $(this).data('aprove-status');

        $.ajax({
            type: 'PUT',
            cache: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                'aproveStatus': aproveStatus
            },
            dataType: "text",
            success: function (data) {
                location.reload();
            },
            error: function (data) {},
        });
    });

    $('#first-slider-modal-cat-select').change(function () {
        console.log('pdr');
        var url = $("#first-slider-modal-cat-select option:selected").data('url');
        $.ajax({
            type: 'get',
            dataType: 'json',
            url: url,
            data: {},
            success: function (data) {
                $('#first-slider-posts-list').html(data);
            },
            async: false
        });
    });



    $(document).on('submit', '#first-slider-welcom-form', function (e) {
        e.preventDefault();
        var id = $('[name="first_slider_prod_check"]:checked').val();
        var url = $(this).data('url');
        console.log(url);
        $.ajax({
            type: 'PUT',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                'id': id
            },
            dataType: "text",
            success: function (data) {
                location.reload();
            },
            error: function (data) {},
        });
    });

    $('.delete-post-first-slide').click(function () {
        var id = $(this).data('id');
        var url = $(this).data('url');
        $.ajax({
            type: 'PUT',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            data: {
                _token: $('meta[name="csrf-token"]').attr('content'),
                'id': id
            },
            dataType: "text",
            success: function (data) {
                location.reload();
            },
            error: function (data) {},
        });
    });


});











// $.ajax({
// 	type: 'get',
// 	dataType: 'json',
// 	url: '/activeDeactiveRow',
// 	data: {
//         'id': id,
//         "project":project 
//     },
// 	success: function (data) {
// 	},
//     async: false
// });



// $.ajax({
//     type: 'POST',
//     headers: {
//     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//     },
//     url: "/changeStatus",
//     data: { _token : $('meta[name="csrf-token"]').attr('content'), 
//         'user':user,
//         'val':val
//     },
//     dataType: "text",
//     success: function(data) {
//     },
//     error: function(data) {      
//     },
// });


// $.ajax({
//     url:url,
//     method:"POST",
//     data:formData,
//     dataType:'JSON',
//     contentType: false,
//     cache: false,
//     processData: false,
//     success:function(data){
//         $('#category-file').val(null);
//     }
