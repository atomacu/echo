@extends('layouts.redactor')


@section('content')
<div class="container">
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Nr.</th>
            <th scope="col">ID</th>
            <th scope="col">Title</th>
            <th scope="col">Created at</th>
        </tr>
        </thead>
        <tbody>
            @foreach (Auth::user()->posts as $i=>$post)
                <tr>      
                    <th scope="row">{{$i+1}}</th>
                    <td>{{$post->id}}</td>
                    <td>{{$post->title}}</td>
                    <td>{{$post->created_at}}</td>
                </tr>
            @endforeach
       
        </tbody>
    </table>
</div>
@endsection

