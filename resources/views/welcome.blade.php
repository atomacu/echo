@extends('layouts.welcome')

@section('content')

<div class="row">
    @foreach ($posts2 as $post)
    <div class="col-md-3 mb-3">
        <a class="link" href="@if(isset($post->seo_link)) {{route('articles.show',$post->seo_link)}} @else {{route('articles.show',$post->id)}} @endif">
            <div class="card border-0 h-100">
                <img style="max-height:300px;" src="/posts/images/{{$post->img}}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">{{$post->title}}</h5>
                    <p class="text-truncate">{!! str_limit(strip_tags($post->description), $limit = 100, $end = '...')
                        !!}</p>
                </div>
            </div>
        </a>
    </div>
    @endforeach
</div>
<div class="my-2">
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <ins class="adsbygoogle" style="display:block; text-align:center;" data-ad-layout="in-article"
        data-ad-format="fluid" data-ad-client="ca-pub-6092609646755605" data-ad-slot="7267933625"></ins>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({});

    </script>
</div>
@if($posts3!==null)
<div class="row">
    <div class="col-md-12 h-100">
        <div class="owl-carousel owl-theme mb-3">
            @foreach ($posts3 as $post)
            <div class="item">
                <a title="{{$post->title}}" class="link" href="@if(isset($post->seo_link)) {{route('articles.show',$post->seo_link)}} @else {{route('articles.show',$post->id)}} @endif">
                    <div class="card">
                        <img style="max-height:300px;" src="/posts/images/{{$post->img}}" class="card-img-top"
                            alt="...">
                        <div class="card-body">
                            <h5 class="card-title">{{$post->title}}</h5>
                            <p class="text-truncate">{!! str_limit(strip_tags($post->description), $limit = 100, $end =
                                '...')
                                !!}
                            </p>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endif
<div class="my-2">
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <ins class="adsbygoogle" style="display:block; text-align:center;" data-ad-layout="in-article"
        data-ad-format="fluid" data-ad-client="ca-pub-6092609646755605" data-ad-slot="7267933625"></ins>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({});

    </script>
</div>
@endsection
