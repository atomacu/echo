@extends('layouts.guest')

@section('content')

<nav aria-label="breadcrumb" class="mt-3">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a title="Home - ECHO"href="{{route('welcome.index')}}">ECHO</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{$category->name}}</li>
    </ol>
</nav>

@if($category->headPost)
<div class="row">
    <div class="col-md-12 mt-1">
        <a title="{{$category->headPost->title}}" href="@if(isset($post->seo_link)) {{route('articles.show',$category->headPost->seo_link)}} @else {{route('articles.show',$category->headPost->id)}} @endif">
            <div class="border-0  card text-dark  font-weight-bold">
                <img src="/posts/images/{{$category->headPost->img2}}" class="vh-50 card-img" alt="...">
                <div class="background-post-user"></div>
                <div class="card-img-overlay">
                    <h1 class="card-title text-white card-title-post">{{$category->headPost->title}}</h1>
                    <p class="h4 align-items-end card-text text-white card-text-post">Adăugat de
                        {{$category->headPost->user->name}}</p>
                </div>
            </div>
        </a>
    </div>
</div>
@endif
<div class="my-2">
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <ins class="adsbygoogle" style="display:block; text-align:center;" data-ad-layout="in-article"
        data-ad-format="fluid" data-ad-client="ca-pub-6092609646755605" data-ad-slot="7267933625"></ins>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({});

    </script>
</div>
<div class="row">
    <div class="col-md-8">
        @if(count($categoryPosts)==0)
        <div class="mt-5 text-center">
            <p class="h2">La moment nu avem postări la această temă.</p>
        </div>
        @else
        <div class="row">
            @foreach ($categoryPosts as $post)
            <div class="col-md-4 my-2">
                <a title="{{$post->title}}" class="link" href="@if(isset($post->seo_link)) {{route('articles.show',$post->seo_link)}} @else {{route('articles.show',$post->id)}} @endif">
                    <div class="card border-0">
                        <img src="/posts/images/{{$post->img}}" height="200px" class="card-img-top" alt="...">
                        <div class="card-body p-1">
                            <h5 class="card-title mb-0">{{$post->title}}</h5>
                        </div>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
        @endif
    </div>
    <div class="col-md-4">
        @foreach ($posts as $i=>$post)
        <a title="{{$post->title}}" href="@if(isset($post->seo_link)) {{route('articles.show',$post->seo_link)}} @else {{route('articles.show',$post->id)}} @endif">
            <div class="mt-2 border-0  card  text-dark font-weight-bold">
                <img src="/posts/images/{{$post->img}}" height="300px" class="card-img" alt="...">
                <div class="background-post-user"></div>
                <div class="card-img-overlay ">
                    <h5 class="card-title text-white card-title-post">{{$post->title}}</h5>
                </div>
            </div>
        </a>
        @if($i!=count($posts)-1)
        <div class="mt-3">
            <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- card -->
            <ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-6092609646755605"
                data-ad-slot="3711831996" data-ad-format="auto" data-full-width-responsive="true"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});

            </script>
        </div>
        @endif
        @endforeach
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        {{ $categoryPosts->onEachSide(0)->links() }}
    </div>
</div>
<div class="my-2">
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <ins class="adsbygoogle" style="display:block; text-align:center;" data-ad-layout="in-article"
        data-ad-format="fluid" data-ad-client="ca-pub-6092609646755605" data-ad-slot="7267933625"></ins>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({});

    </script>
</div>
@endsection
