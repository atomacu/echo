<div class="row">
    @foreach ($posts as $post)
        <div class="col-md-4 mt-1">
            <div class="card">
                <img src="/posts/images/{{$post->img}}" height="150px" class="card-img-top" alt="...">
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">{{$post->title}}</li>
                    <li class="text-center list-group-item">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="first_slider_prod_check" id="first-slider-prod-{{$post->id}}" value="{{$post->id}}">
                            <label class="form-check-label" for="first-slider-prod-{{$post->id}}">
                            </label>
                        </div>
                    </li>
                </ul>
            </div>
        </div> 
    @endforeach
</div>