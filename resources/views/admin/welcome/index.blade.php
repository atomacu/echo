@extends('layouts.'.Auth::user()->userRole->role->default_page)
@section('content')
<div class="container-fluid">
    <div class="row px-2">
        <div class="col-md-4">
            <p class="pl-3 h3">
                <i class="fas fa-truck-pickup mr-2"></i>First slider
            </p>
        </div>
        <div class="col-md-8">
            <button data-toggle="modal" data-target="#first-slide-modal"
                class="round-btn-primary btn-circle float-right"><i class="fas fa-plus"></i></button>

            <div class="modal fade" id="first-slide-modal" tabindex="-1" role="dialog"
                aria-labelledby="first-slide-modal-title" aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="first-slide-modal-title">First slider</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            <form method="POST" enctype="multipart/form-data" id="first-slider-welcom-form"
                                data-url="{{route('welcomeset.updateFirstSlider',1)}}">

                                <div class="row justify-content-center">
                                    <div class="col-md-10">
                                        <select id="first-slider-modal-cat-select" class="form-control">
                                            <option selected disabled>Select Category</option>
                                            @foreach ($categories as $category)
                                            <option data-url="{{route('welcomeset.firstSlidetGet',$category->id)}}">
                                                {{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="mt-3 row justify-content-center">
                                    <div class="col-md-10" id="first-slider-posts-list">

                                    </div>
                                </div>
                                <div class="row mt-3 justify-content-center">
                                    <div class="col-md-10">
                                        <button type="submit" class="btn-block btn btn-primary"><i
                                                class="fas fa-check mr-2"></i>Add</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        @foreach ($posts1 as $post)
        <div class="col-md-4 mt-3">
            <a href="{{route('post.show', $post->id)}}">
                <div class="card border-0 bg-dark text-dark font-weight-bold">
                    <img src="/posts/images/{{$post->img}}" height="300px" class="card-img" alt="...">
                    <div class="background-post">
                    </div>
                    <div class="card-img-overlay ">
                        <h5 class="card-title card-title-post">{{$post->title}}</h5>
                        <p class="align-items-end card-text card-text-post">Adăugat de {{$post->user->name}}</p>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-md-12">
                            <button data-id="{{$post->id}}" data-url="{{route('welcomeset.updateFirstSlider',0)}}"
                                class="btn btn-danger delete-post-first-slide btn-block">Delete</button>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endforeach
    </div>
</div>
<hr>
<div class="row px-2">
    <div class="col-md-4">
        <p class="pl-3 h3">
            <i class="fas fa-truck-pickup mr-2"></i>Second slider(4 posts)
        </p>
    </div>
    <div class="col-md-8">
        <button data-toggle="modal" data-target="#first-slide-modal" class="round-btn-primary btn-circle float-right"><i
                class="fas fa-plus"></i></button>

        <div class="modal fade" id="first-slide-modal" tabindex="-1" role="dialog"
            aria-labelledby="first-slide-modal-title" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="first-slide-modal-title">First slider</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <form method="POST" enctype="multipart/form-data" id="first-slider-welcom-form"
                            data-url="{{route('welcomeset.updateFirstSlider',1)}}">

                            <div class="row justify-content-center">
                                <div class="col-md-10">
                                    <select id="first-slider-modal-cat-select" class="form-control">
                                        <option selected disabled>Select Category</option>
                                        @foreach ($categories as $category)
                                        <option data-url="{{route('welcomeset.firstSlidetGet',$category->id)}}">
                                            {{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="mt-3 row justify-content-center">
                                <div class="col-md-10" id="first-slider-posts-list">

                                </div>
                            </div>
                            <div class="row mt-3 justify-content-center">
                                <div class="col-md-10">
                                    <button type="submit" class="btn-block btn btn-primary"><i
                                            class="fas fa-check mr-2"></i>Add</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
@endsection
