@extends('layouts.'.Auth::user()->userRole->role->default_page)
@section('content')

<div class="container">

    <nav aria-label="breadcrumb" class="mt-3">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('welcome.index')}}">ECHO</a></li>
            <li class="breadcrumb-item"><a href="{{route('ucategory.show',$post->category->id)}}">{{$post->category->name}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{$post->title}}</li>
        </ol>
    </nav>
    <div class="row mb-3">
        <div class="col-md-12">
            <button data-url="{{route('post.destroy',$post->id)}}" type="button" id="delete-btn-post" class="btn link btn-danger btn-block">Delete</button>
        </div>
    </div>
    @if($post->draft==1)
        <div class="row">
            <div class="col-md-12">
                <button id="post-draft-btn" data-url="{{route('draft.update',$post->id)}}" class="btn-primary btn btn-block"><i class="fas fa-file-alt mr-2"></i>Post</button>
            </div>
        </div>
    @endif
    <div class="row">

        <div class="col-lg-8">
            <h1 class="mt-4 text-indent">{{$post->title}} <a href="{{route('post.edit', $post->id)}}"><i class="fas fa-pencil-alt"></i></a></h1> 
            <p class="lead">
                de <a href="#">{{$post->user->name}}</a>
                <span class="float-right">Postat pe {{date_format($post->created_at, 'd.m.y , g:i a')}}</span>
            </p>
            <img class="w-100 max-vh-50 img-fluid" src="/posts/images/{{$post->img2}}" alt="">
            <hr>
            <div class="text-indent">
                {!!$post->description!!}
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="fb-comments" data-href="{{route(Route::currentRouteName(),$post->id)}}" data-width="100%" data-numposts="5"></div>
                </div>
            </div> 
        </div>
        <div class="col-md-4 mt-5">
            @foreach ($posts as $post)
                <a href="{{route('post.show', $post->id)}}">
                    <div class="mb-3 border-0 card bg-dark text-dark font-weight-bold">
                        <img src="/posts/images/{{$post->img}}" height="300" class="card-img" alt="...">
                        <div class="background-post">
                        </div>
                        <div class="card-img-overlay ">
                            <h3 class="card-title card-title-post">{{$post->title}}</h3>
                            <p class="align-items-end card-text card-text-post">Adăugat de {{$post->user->name}}</p>
                        </div>
                    </div>
                </a>
            @endforeach
        </div>

    </div>
</div>

@endsection
