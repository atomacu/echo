@extends('layouts.admin')
@section('content')

    <div class="container add-post-form">
    <form  method="POST" enctype="multipart/form-data" data-draft="0" id='add-post-form' data-url="{{route('post.store')}}" data-id="{{$id}}">
        @method('POST')
        {!! csrf_field() !!}
        <div class="row">
            <div class="col-md-12">
                <textarea type="text" class="form-control" placeholder="Titlul" id='post-title' required></textarea>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="h4">Aspect Ratio 1:1 <span style="color:brown; font-weight:bold;">required</span></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <input type="file" name='image' class='mt-3 main-image' onchange='readURL(this);' required>
            </div>
        </div>

        <div class="img-div row">
            <div class="col-md-12">
                <button type="button" class="btn offset-11 btn-lg d-none close-img-btn "><i
                        class="fas fa-times"></i></button>
                <img id="image-file" src="#" alt="Image" class='d-none mt-1 main-img-style' />
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="h4">Aspect Ratio 3:1 <span style="color:brown; font-weight:bold;">required</span></p>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12">
                <input type="file" name='image_2' class='mt-3 main-image2' onchange='readURL2(this);' required>
            </div>
        </div>

        <div class="img-div2 row">
            <div class="col-md-12">
                <button type="button" class="btn offset-11 btn-lg d-none close-img-btn2 "><i
                        class="fas fa-times"></i></button>
                <img height="auto" id="image-file2" src="#" alt="Image" class='d-none max-vh-50 mt-1 main-img-style' />
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <hr>
            </div>
        </div>
      
        <div class="mt-3 row">
            <div class="col-md-12">
                <textarea name="content" id="editor"></textarea>
            </div>
        </div>
        <div class="row my-3">
            <div class="col-md-6">
                <button class="btn btn-warning float-right btn-block" id="add-post-draft" type="submit"><i class="fas fa-pencil-ruler mr-2"></i>Draft</button>
            </div>
            <div class="col-md-6">
                <button class="btn btn-primary btn-block float-right ml-2" id="add-post-btn-add" type="submit"><i class="fas fa-file-alt mr-2"></i>Add</button>
            </div>
        </div>
    </form>
    
   
    </div>
   
@endsection