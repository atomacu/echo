@extends('layouts.'.Auth::user()->userRole->role->default_page)
@section('content')

<div class="container">
    <form enctype="multipart/form-data" data-draft="0" id='update-post-form' data-url="{{route('post.update',$post->id)}}">
        @method('PATCH')
        {{-- @csrf --}}
        <div class="row">
            <div class="col-md-12">
                <textarea type="text" class="form-control" placeholder="Titlul" id='post-title'>{{$post->title}}</textarea>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <input type="file" name='image' class='mt-3 main-image' onchange='readURL(this);'>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <p class="h4">Aspect Ratio 1:1</p>
            </div>
        </div>

        <div class="img-div row">
            <div class="col-md-12">
                <img id="image-file" style="width:350px; height:350px;" src="/posts/images/{{$post->img}}" alt="Image" class='max-vh-50 mt-1 main-img-style' />
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <p class="h4">Aspect Ratio 3:1</p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <input type="file" name='image_2' class='mt-3 main-image2' onchange='readURL2(this);'>
            </div>
        </div>

        <div class="img-div2 row">
            <div class="col-md-12">
                <img height="auto" id="image-file2" src="/posts/images/{{$post->img2}}" alt="Image" class='w-100 max-vh-50 mt-1 main-img-style' />
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <hr>
            </div>
        </div>
      
        <div class="mt-3 row">
            <div class="col-md-12">
                <textarea name="content" id="editor">{{$post->description}}</textarea>
            </div>
        </div>
        <div class="row my-3">
            <div class="col-md-12">
                <button class="btn btn-primary float-right ml-2 btn-block" type="submit"><i class="fas fa-save mr-2"></i>Save</button>
            </div>
        </div>
    </form>
</div>
   
@endsection