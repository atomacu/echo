@extends('layouts.'.Auth::user()->userRole->role->default_page)
@section('content')
<div class="container-fluid">

    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link py-2 px-2 active" href="">Postări</a>
        </li>
        <li class="nav-item">
            <a class="nav-link py-2 px-2" href="{{route('draft.show',$category->id)}}">Draft</a>
        </li>
        <li class="nav-item">
            <a class="nav-link py-2 px-2" href="{{route('aprove.showRejected',$category->id)}}">Neaprobate</a>
        </li>
        <li class="nav-item">
            <a class="nav-link py-2 px-2" href="{{route('aprove.show',$category->id)}}">În proces</a>
        </li>
    </ul>
    <div class="row mt-2">
        <div class="col-md-12">
            <a href={{route('post.create', $id)}} class='round-btn-primary float-right btn-circle'><i class="fas fa-plus"></i></a>
        </div>
    </div>
    
    <div class="row mb-3">
        @foreach ($posts as $post)
            <div class="col-md-4 mt-4">
                <a href="{{route('post.show', $post->id)}}">
                    <div class="card border-0 bg-dark text-dark font-weight-bold">
                        <img src="/posts/images/{{$post->img}}" height="300px" class="card-img" alt="...">
                        <div class="background-post">
                        </div>
                        <div class="card-img-overlay ">
                            <h5 class="card-title card-title-post">{{$post->title}}</h5>
                            <p class="align-items-end card-text card-text-post">Adăugat de {{$post->user->name}}</p>
                           @if(Auth::user()->userRole->role->default_page=="admin")
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="custom-control custom-switch">
                                        <input data-url="{{route('post.setOnCategory',$post->id)}}" type="checkbox" class="custom-control-input switch-post" data-id="{{$post->id}}" id="switch-post-{{$post->id}}" @if($post->category_head==1) checked @endif>
                                        <label class="custom-control-label text-white" for="switch-post-{{$post->id}}">Setează la început</label>
                                    </div>
                                </div>
                            </div>
                            @endif

                        </div>
                    </div>
                </a>
            </div>
        @endforeach
    </div>
    <div class="row">
        <div class="col-md-4">
            {{ $posts->onEachSide(0)->links() }}
        </div>
    </div>
</div>

@endsection
