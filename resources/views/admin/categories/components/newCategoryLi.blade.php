
<a title="{{$category->name}}" href="{{route('category.show', $category->id)}}" class="list-group-item list-group-item-action text-white text-center bg-dark">{{ $category->name }}</a>