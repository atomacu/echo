@extends('layouts.'.Auth::user()->userRole->role->default_page)
@section('content')
<div class="container-fluid">
    <div class="row mb-3">
        @if(count($posts)==0)
        <div class="col-md-12 mt-5 text-center">
            <p class="h2">Nici o postare nouă.</p>
        </div>
        @else
            @foreach ($posts as $post)
                <div class="col-md-4 mt-4">
                    <div class="card border-0 bg-dark text-dark font-weight-bold">
                        <a href="{{route('post.show', $post->id)}}">
                                <img src="/posts/images/{{$post->img}}" height="300px" class="card-img" alt="...">
                            <div class="background-post">
                            </div>
                            <div class="card-img-overlay ">
                                <h3 class="card-title card-title-post">{{$post->title}}</h3>
                                <p class="align-items-end card-text card-text-post">Domeniu: {{$post->category->name}}</p>
                                <p class="align-items-end card-text card-text-post">Adăugat de {{$post->user->name}}</p>
                            </div>
                        </a>
                        <div class="card-body p-0">
                            <div class="row">
                                <div class="col-md-6 pr-0">
                                    <button data-url="{{route('aprove.update',$post->id)}}" data-aprove-status="2" class="btn btn-danger aprove-post-btn btn-block">Reject</button>
                                </div>
                                <div class="col-md-6 pl-0">
                                    <button data-url="{{route('aprove.update',$post->id)}}" data-aprove-status="1" class="btn btn-primary aprove-post-btn btn-block">Aprove</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>
    <div class="row">
        <div class="col-md-4">
            {{ $posts->onEachSide(0)->links() }}
        </div>
    </div>
</div>
@endsection
