<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ECHO</title>
    <link rel="shortcut icon" href="/img/logo-title.png" />
    <link href="{{asset('css/fontawesome-free/all.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap" rel="stylesheet">
    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/cropper.css')}}">

</head>

<body>
    <div id="app">
        <div class="fixed-top" id="side-navbar-animation wrapper">
            <div class="pb-3 bg-dark" id="sidebar-wrapper">
                <div class="sidebar-heading text-white">
                    <a href="{{route('welcome.index')}}"><img width="100%" src="/img/echo-logo.png" alt=""></a>
                </div>
                <div class="list-group list-group-flush">
                    <a href="{{route('home')}}" class="list-group-item list-group-item-action text-white text-center bg-dark  @if('admin.index'==Route::currentRouteName()) nav-active @endif">
                        <i class="fas fa-home mr-2"></i>Home</a>
                    <a href="{{route('aprove.index')}}" class="list-group-item list-group-item-action text-white text-center bg-dark @if('aprove.index'==Route::currentRouteName()) nav-active @endif">
                        <i class="fas fa-thumbs-up mr-2"></i>Articles approval
                    </a>
                    <a href="{{route('welcomeset.index')}}" class="list-group-item list-group-item-action text-white text-center bg-dark @if('welcomeset.index'==Route::currentRouteName()) nav-active @endif">
                        <i class="fas fa-cogs mr-2"></i>Welcome settings
                    </a>
                    <a href="{{route('category.index')}}" class="list-group-item list-group-item-action text-white text-center mb-0 pb-0 bg-dark @if('category.index'==Route::currentRouteName()) nav-active @endif">
                        <i class="fas fa-boxes mr-2"></i>Categories managment
                        <hr class="sidebar-divider">
                    </a>
                    <div class="categories">
                        @foreach ($categories as $i=>$category)
                            <a href="{{route('category.show', $category->id)}}" class="@if(count($categories)==$i+1) mb-1 @endif list-group-item list-group-item-action text-white text-center bg-dark  @if(isset($id))@if($category->id==$id) nav-active @endif @endif">
                                {{ $category->name }}
                                @if(count($categories)==$i+1)
                                    <hr class="mb-1 sidebar-divider"> 
                                @endif
                            </a>
                        @endforeach
                    </div>
                </div>
                <div class="input-group mb-3 p-3 d-none" id="input-category" data-toggle="0">
                    <input type="text" class="form-control" aria-label="Categorie" id="category-input">
                    <div class="input-group-append">
                        <button class="btn btn-secondary" type="button" id="add-category-btn"
                            data-url={{route('category.store')}}>Add</button>
                    </div>
                </div>
                <div class="text-center">
                    <button class="round-btn-primary btn-circle" id="plusButton"><i class="fas fa-plus"></i></button>
                </div>
            </div>
            <div id="top-white-navbar">
                <div id="content">
                    <nav class="navbar navbar-expand navbar-light bg-white topbar static-top shadow">
                        <ul class="navbar-nav ml-auto">
                            <form action="{{route('logout')}}" method="POST" class="d-flex p-1">
                                @csrf
                                <button type='submit' class="btn btn-primary">Logout</button>
                            </form>
                        </ul>
                    </nav>
                </div>
            </div>
            <div id="page-content-wrapper">
                <nav class="navbar fixed-top navbar-expand-lg navbar-dark text-white bg-dark border-bottom d-md-none">
                    <a href="{{route('welcome.index')}}" class="navbar-brand">ECHO</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                            <li class="nav-item">
                                <a href="{{route('home')}}" class="nav-link @if('admin.index'==Route::currentRouteName()) active @endif"><i class="fas fa-home mr-2"></i>Home</a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('category.index')}}" class="nav-link @if('category.index'==Route::currentRouteName()) active @endif">
                                    <i class="fas fa-boxes mr-2"></i>Categories managment
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('welcomeset.index')}}" class="nav-link @if('welcomeset.index'==Route::currentRouteName()) active @endif">
                                    <i class="fas fa-cogs mr-2"></i>Welcome settings
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{route('aprove.index')}}" class="nav-link @if('aprove.index'==Route::currentRouteName()) active @endif">
                                        <i class="fas fa-thumbs-up mr-2"></i>Articles approval
                                </a>
                            </li>
                            @foreach ($categories as $category)
                            <li class="nav-item">
                                <a href={{route('category.show', $category->id)}} class="nav-link @if(isset($id))@if($category->id==$id) active @endif @endif">
                                    {{ $category->name }}
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <div class="page-wrapper">
            <div class="min-vh-100">
                @yield('content')
            </div>
            <footer class="py-5 bg-dark">
                <div class="container">
                    <p class="m-0 text-center text-white">Copyright &copy; <a href="" target="_blank">SoftChamp Inc</a> 2019</p>
                </div>
            </footer>
        </div>
    </div>
    <script src="/js/jquery/jquery.min.js"></script>
    <script src="/js/bootstrap/bootstrap.bundle.min.js"></script>
    <script src="/js/jquery/jquery.easing.min.js"></script>
    <script src="/js/sb-admin-2.min.js"></script>
    <script src="{{asset('js/script.js')}}"></script>
    <script src="{{asset('js/cropper.js')}}"></script>
    <script src="{{asset('js/app.js')}}"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous"
        src="https://connect.facebook.net/ro_RO/sdk.js#xfbml=1&version=v3.3&appId=406720239939704&autoLogAppEvents=1">
    </script>
</body>
</html>
