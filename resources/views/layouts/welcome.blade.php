<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="og:type" content="website">
    <meta property="type"            content="website" /> 
    <meta property="og:url"             content="{{URL::to('/')}}" /> 
    <meta property="url"             content="{{URL::to('/')}}" /> 
    <meta property="og:title"           content="ECHO" /> 
    <meta property="title"           content="ECHO" /> 
    <meta property="twitter:image"  content="{{URL::to('/')}}/img/facebook-cover.png"/>
    <meta property="og:image" content="{{URL::to('/')}}/img/facebook-cover.png"/>
    <meta property="twitter:title" content="ECHO"/>
    <meta property="twitter:description"  content="Suntem un portal de știri din Republica Moldova, articolele noastre sunt despre cele mai interesante evenimente și noutati din Republica Moldova și intreaga lume. Temele pe care ne străduim să le abordam sunt tehnologiile și businessul."/>
    <meta property="og:description"    content="Suntem un portal de știri din Republica Moldova, articolele noastre sunt despre cele mai interesante evenimente și noutati din Republica Moldova și intreaga lume. Temele pe care ne străduim să le abordam sunt tehnologiile și businessul. " />
    <meta property="description"    content="Suntem un portal de știri din Republica Moldova, articolele noastre sunt despre cele mai interesante evenimente și noutati din Republica Moldova și intreaga lume. Temele pe care ne străduim să le abordam sunt tehnologiile și businessul. " />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>ECHO</title>
    <link rel="shortcut icon" href="/img/logo-title.png" />

    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}" defer></script> --}}
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <link href="{{asset('css/fontawesome-free/all.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap" rel="stylesheet">
    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
    <link href="{{ asset('css/guest-style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/docs.theme.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-6092609646755605",
            enable_page_level_ads: true
        });
    </script>
   
</head>

<body>



    <div id="app">
        <div class="fixed-top" id="side-navbar-animation wrapper">
            <div class="bg-dark" id="sidebar-wrapper">
                <div class="sidebar-heading text-white">
                    <a title="HOME - ECHO" href="{{route('welcome.index')}}"><img width="100%" src="/img/echo-logo.png" alt=""></a>
                </div>
                <div class="list-group list-group-flush">
                    @if(Auth::user())
                        <a title="Home" href="{{route('home')}}" class="list-group-item list-group-item-action text-white text-center bg-dark  @if('home'==Route::currentRouteName()) nav-active @endif"><i class="fas fa-home mr-2"></i>Home</a>
                    @endif
                    @foreach ($categories as $category)
                        <a title="{{ $category->name }}" href="{{route('ucategory.show', $category->id)}}" class="list-group-item list-group-item-action text-white text-center bg-dark">{{ $category->name }}</a>
                    @endforeach

                </div>
            </div>


            <div id="page-content-wrapper">
                <nav class="navbar fixed-top navbar-expand-lg navbar-dark text-white bg-dark border-bottom d-md-none">
                    <a title="Home" class="navbar-brand">ECHO</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                            <li class="nav-item @if('home'==Route::currentRouteName()) active @endif">
                                @if(Auth::user())
                                    <a title="Home" href="{{route('home')}}" class="nav-link"><i class="fas fa-home mr-2"></i>Home</a>
                                @endif
                                <a title="{{$category->name}}" href={{route('ucategory.show', $category->id)}} >
                                    {{ $category->name }}
                                </a>
                            </li>
                            @foreach ($categories as $category)
                            <li class="nav-item ">
                                <a title="{{ $category->name }}" href={{route('ucategory.show', $category->id)}} class="nav-link ">
                                    {{ $category->name }}
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <div class="page-wrapper">
            <div id="carouselExampleIndicators" class="mb-3 carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    @foreach ($posts1 as $i=>$post)
                        <li data-target="#carouselExampleIndicators" data-slide-to="{{$i}}" class="@if($i==0) active @endif"></li>
                    @endforeach
                </ol>
                <div class=" carousel-inner">
                    @foreach($posts1 as $i=>$post)
                        <div class="carousel-item @if($i==0) active @endif">
                            <a title="{{$post->title}}" class="link" href="@if(isset($post->seo_link)) {{route('articles.show',$post->seo_link)}} @else {{route('articles.show',$post->id)}} @endif">
                                <img src="/posts/images/{{$post->img2}}" class="d-block vh-60 w-100" alt="...">
                                <div class="background-post-user"></div>
                                <div class="carousel-caption d-none d-md-block">
                                    <h5>{{$post->title}}</h5>
                                    <p class="text-truncate">{!! str_limit(strip_tags($post->description), $limit = 250, $end = '...')  !!}</p>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
                <a title="Previous" class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a title="Next" class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <div class="container-fluid">
                @yield('content')
            </div>
            <footer class="py-5 bg-dark">
                <div class="container">
                    <p class="m-0 text-center text-white">
                        Copyright &copy; <a href="" target="_blank">SoftChamp Inc</a> 2019
                    </p>
                    <div class="fb-share-button" data-href="https://echo.md/" data-layout="button_count"></div>
                </div>

            </footer>
        </div>

    </div>

    <!-- Global site tag (gtag.js) - Google Analytics -->

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-143374596-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-143374596-1');

    </script>
    <div id="fb-root"></div>
    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.4&appId=241110544128";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

    </script>
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-6092609646755605",
        enable_page_level_ads: true
    });
    </script>
     <script async custom-element="amp-auto-ads"
        src="https://cdn.ampproject.org/v0/amp-auto-ads-0.1.js">
    </script>
    <amp-auto-ads type="adsense"
        data-ad-client="ca-pub-6092609646755605">
    </amp-auto-ads>
    <script src="{{ asset('js/jquery.min.js') }}" defer></script>
    <script src="{{ asset('js/highlight.js') }}" defer></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}" defer></script>
    <script src="{{ asset('js/owl.carousel.js') }}" defer></script>
    <script src="{{ asset('js/cscript.js') }}" defer></script>

</body>

</html>
