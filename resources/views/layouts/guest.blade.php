<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @if(isset($postPage))
        {{-- <meta property="fb:app_id"          content="{{str_random(32)}}" />  --}}
        <meta property="og:type"            content="website" /> 
        <meta property="og:url"             content="{{route(Route::currentRouteName(),$postPage->id)}}" /> 
        <meta property="og:title"           content="{{$postPage->title}}" /> 
        <meta property="og:image"           content="{{URL::to('/')}}/posts/images/{{$postPage->img2}}" /> 
        <meta property="twitter:image"  content="{{URL::to('/')}}/posts/images/{{$postPage->img2}}">
        <meta property="og:description"    content="{{ strip_tags($postPage->description) }}" />
        <meta property="description"    content="{{ strip_tags($postPage->description) }}" />
        <meta property="twitter:title" content="{{$postPage->title}}"/>
        <meta property="twitter:description"  content="{{ strip_tags($postPage->description) }}"/>
    @else
        <meta property="type"            content="website" /> 
        <meta property="og:type"            content="website" /> 
        <meta property="og:url"             content="{{URL::to('/')}}" /> 
        <meta property="url"             content="{{URL::to('/')}}" /> 
        <meta property="og:title"           content="ECHO" /> 
        <meta property="title"           content="ECHO" /> 
        <meta property="twitter:image"  content="{{URL::to('/')}}/img/facebook-cover.png">
        <meta property="og:image" content="{{URL::to('/')}}/img/facebook-cover.png">
        <meta property="og:description"    content="Suntem un portal de știri din Republica Moldova, articolele noastre sunt despre cele mai interesante evenimente și noutati din Republica Moldova și intreaga lume. Temele pe care ne străduim să le abordam sunt tehnologiile și businessul. " />
        <meta property="description"    content="Suntem un portal de știri din Republica Moldova, articolele noastre sunt despre cele mai interesante evenimente și noutati din Republica Moldova și intreaga lume. Temele pe care ne străduim să le abordam sunt tehnologiile și businessul. " />
        <meta property="twitter:title" content="ECHO"/>
        <meta property="twitter:description"  content="Suntem un portal de știri din Republica Moldova, articolele noastre sunt despre cele mai interesante evenimente și noutati din Republica Moldova și intreaga lume. Temele pe care ne străduim să le abordam sunt tehnologiile și businessul."/>
    @endif
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>ECHO</title>
    <link rel="shortcut icon" href="/img/logo-title.png" />
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="{{asset('css/fontawesome-free/all.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/guest-style.css') }}" rel="stylesheet">
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-6092609646755605",
            enable_page_level_ads: true
        });
    </script>

</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a title="HOME - ECHO" class="navbar-brand" href="{{route('welcome.index')}}"><img width="150px" src="/img/echo-logo.png" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    @if(Auth::user())
                        <li class="nav-item">
                            <a title="Home" href="{{route('home')}}" class="nav-link"><i class="fas fa-home mr-2"></i>Home</a>
                        </li>
                    @endif
                    @foreach ($categories as $category)
                        <li class="nav-item @if(isset($id))@if($category->id==$id) active @endif @endif">
                            <a title="{{ $category->name }}" href="{{route('ucategory.show', $category->id)}}" class="nav-link">{{ $category->name }}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </nav>

    <div id="app">
        <div class="container">
            <div class="min-vh-100">
                @yield('content')
            </div>
        </div>
        <footer class="mt-3 py-5 bg-dark">
            <div class="container">
                <p class="m-0 text-center text-white">
                    Copyright &copy; <a href="" target="_blank">SoftChamp Inc</a> 2019
                </p>
                <div class="fb-share-button" data-href="https://echo.md/" data-layout="button_count"></div>
            </div>
        </footer>
    </div>

    <!-- Global site tag (gtag.js) - Google Analytics -->

    <div id="fb-root"></div>
    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.4&appId=241110544128";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

    </script>
    <script src="{{asset('js/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('js/cscript.js')}}"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-143374596-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-143374596-1');

    </script>
    <div id="fb-root"></div>
    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.4&appId=241110544128";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

    </script>

    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
    (adsbygoogle = window.adsbygoogle || []).push({
        google_ad_client: "ca-pub-6092609646755605",
        enable_page_level_ads: true
    });
    </script>
    <script async custom-element="amp-auto-ads"
        src="https://cdn.ampproject.org/v0/amp-auto-ads-0.1.js">
    </script>
    <amp-auto-ads type="adsense"
        data-ad-client="ca-pub-6092609646755605">
    </amp-auto-ads>
</body>

</html>
