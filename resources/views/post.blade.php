@extends('layouts.guest')

@section('content')
<nav aria-label="breadcrumb" class="mt-3">
    <ol class="breadcrumb list-inline">
        <li class="breadcrumb-item list-inline-item"><a href="{{route('welcome.index')}}">ECHO</a></li>
        <li class="breadcrumb-item list-inline-item"><a
                href="{{route('ucategory.show',$postPage->category->id)}}">{{$postPage->category->name}}</a></li>
        <li class="breadcrumb-item w-50 list-inline-item active text-truncate" aria-current="page">{{$postPage->title}}
        </li>
    </ol>
</nav>
<div class="row">
    <div class="col-lg-8">
        <div class="mt-5">
            <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <ins class="adsbygoogle" style="display:block; text-align:center;" data-ad-layout="in-article"
                data-ad-format="fluid" data-ad-client="ca-pub-6092609646755605" data-ad-slot="7267933625"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});

            </script>
        </div>
        <h1 class="mt-4 text-indent">{{$postPage->title}}</h1>
        <p class="lead">
            de <a title="{{$postPage->user->name}}"  href="#">{{$postPage->user->name}}</a>
            <span class="float-right">Postat pe {{date_format($postPage->created_at, 'd.m.y , g:i a')}}</span>
        </p>
        <img class="w-100 max-vh-50 img-fluid" src="/posts/images/{{$postPage->img2}}" alt="">
        <hr>
        <div class="text-indent">
            {!!$postPage->description!!}
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="fb-comments" data-href="{{route(Route::currentRouteName(),$postPage->id)}}"
                    data-width="100%" data-numposts="5"></div>
            </div>
        </div>
        <div class="my-2">
            <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <ins class="adsbygoogle" style="display:block; text-align:center;" data-ad-layout="in-article"
                data-ad-format="fluid" data-ad-client="ca-pub-6092609646755605" data-ad-slot="7267933625"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});

            </script>
        </div>
    </div>
    <div class="col-md-4 mt-4">
        @foreach ($posts as $i=>$post)
        <a title="{{$post->title}}" class="link" href="@if(isset($post->seo_link)) {{route('articles.show',$post->seo_link)}} @else {{route('articles.show',$post->id)}} @endif">
            <div class="mt-3 card border-0">
                <img src="/posts/images/{{$post->img}}" height="300px" class="card-img-top" alt="...">
                <div class="card-body p-1">
                    <h5 class="card-title mb-0">{{$post->title}}</h5>
                </div>
            </div>
        </a>
        @if($i!=count($posts)-1)
        <div class="mt-3">
            <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- card -->
            <ins class="adsbygoogle" style="display:block" data-ad-client="ca-pub-6092609646755605"
                data-ad-slot="3711831996" data-ad-format="auto" data-full-width-responsive="true"></ins>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({});

            </script>
        </div>
        @endif
        @endforeach
    </div>
</div>
@endsection
