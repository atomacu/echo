<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    protected $fillable = [
        'img'
    ];

    static function setImg($id,$new_name){
        parent::where('id',$id)->update([
            'img'=>$new_name
        ]);
        return 1;
    }
}
