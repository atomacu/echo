<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;
    
    // const table = 'posts';
    // protected $table = self::table;
    // protected $primaryKey = 'id';

    protected $fillable = [
        'title','description','cat_id','img','user_id','draft'
    ];

    protected $casts = [
        'created_at' => 'datetime:d-month-Y',
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function category(){
        return $this->belongsTo('App\Category','cat_id');
    }
}
