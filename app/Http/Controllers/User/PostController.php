<?php

namespace App\Http\Controllers\User;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $postPage=Post::find($id);
        if($postPage){
            $idString = (string)$postPage->id;
            $title=str_replace("?","", str_replace("\\","", str_replace("/","",strtolower ($postPage->title))))."-echo-".$idString  ;
            $title = explode(" ", $title);
            $newSeoTitle = "";
            foreach($title as $t){
                if(count($title)==1){

                    $newSeoTitle = $t;
                }elseif($newSeoTitle == ""){
                    $newSeoTitle = $newSeoTitle.$t."-";
                }else{
                    $newSeoTitle = $newSeoTitle."-".$t;
                }
            }
        
            Post::where('id',$id)->update([
                "seo_link" =>$newSeoTitle
            ]);
        }else{
            $postPage=Post::where('seo_link',$id)->first();
        }
        $posts=Post::inRandomOrder()->where('draft',0)->where('aprove',1)->where('category_head',0)->get();
        if(count($posts)>=4){
            $posts=Post::inRandomOrder()->where('draft',0)->where('aprove',1)->where('category_head',0)->take(3)->get();
        }
        return view('post',compact('postPage','posts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
