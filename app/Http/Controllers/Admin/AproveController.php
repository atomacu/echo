<?php

namespace App\Http\Controllers\Admin;

use App\Post;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AproveController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts=Post::where('aprove',0)->where('draft',0)->paginate(6);
        return view('admin.aprove.index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category=Category::find($id);
        $posts=Post::where('cat_id',$category->id)->where('aprove',0)->where('draft',0)->paginate(6);
        return view("admin.aprove.inProces",compact('posts','category'));
    }

    public function showRejected($id)
    {
        $category=Category::find($id);
        $posts=Post::where('cat_id',$category->id)->where('aprove',2)->where('draft',0)->paginate(6);
        return view("admin.aprove.rejected",compact('posts','category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request['aproveStatus']==1){
            Post::where('id',$id)->update([
                'aprove'=>1
            ]);
        }elseif($request['aproveStatus']==2){
            Post::where('id',$id)->update([
                'aprove'=>2
            ]);
        }elseif($request['aproveStatus']==0){
            Post::where('id',$id)->update([
                'aprove'=>0
            ]);
        }
        return 1;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
