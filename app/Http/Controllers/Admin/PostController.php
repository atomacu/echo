<?php

namespace App\Http\Controllers\Admin;

use Auth;
use File;
use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request )
    {
        dd($request->all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('admin.categories.posts.create', compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = Post::create([
            'title' => $request['title'],
            'description' => $request['description'],
            'cat_id' => $request['catID'],
            'user_id' => Auth::user()->id,
            'draft'=>$request['draft']
        ]);

        $idString = (string)$post->id;
        $title=str_replace("?","", str_replace("\\","", str_replace("/","",strtolower ($postPage->title))))."-echo-".$idString  ;
        $title = explode(" ", $title);
        $newSeoTitle = "";
        foreach($title as $t){
            if($newSeoTitle == ""){
                $newSeoTitle = $newSeoTitle.$t."-";
            }else{
                $newSeoTitle = $newSeoTitle."-".$t;
            }
        }
       
        Post::where('id',$post->id)->update([
            "seo_link" =>$newSeoTitle
        ]);
    

        $image1 = $request['img1'];  
        $virgula=strrpos($image1,',');
        $str=substr($image1, 0,$virgula+1);
        $bara=strrpos($image1,'image/');
        $punct=strrpos($image1,';base64');
        $extension= substr($str, $bara+6,$punct-$bara-6);
        $image1 = str_replace('data:image/'.$extension.';base64,', '', $image1);
        $image1 = str_replace(' ', '+', $image1);

        $imageName = 'post-'.$post->id.'-base.'.$extension;
       
        File::put('posts/images/'. $imageName, base64_decode($image1));
        
        $post->img=$imageName;
        $post->save();
        
       

        $image2 = $request['img2'];  
        $virgula=strrpos($image2,',');
        $str=substr($image2, 0,$virgula+1);
        $bara=strrpos($image2,'image/');
        $punct=strrpos($image2,';base64');
        $extension= substr($str, $bara+6,$punct-$bara-6);
        $image2 = str_replace('data:image/'.$extension.';base64,', '', $image2);
        $image2 = str_replace(' ', '+', $image2);

        $imageName2 = 'post-'.$post->id.'.'.$extension;
        File::put('posts/images/'. $imageName2, base64_decode($image2));
        
        $post->img2=$imageName2;
        $post->save();
        
        if($request['draft']==1){
            return route('draft.show', $request['catID']);
        }else{
            return route('category.show', $request['catID']);
        }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post=Post::find($id);
        $posts=Post::where('cat_id',$post->cat_id)->where('aprove',1)->where('id','<>',$id)->orderBy('updated_at', 'desc')->get();
        if(count($posts)>=4){
            $posts=Post::where('cat_id',$post->cat_id)->where('aprove',1)->where('id','<>',$id)->orderBy('updated_at', 'desc')->take(3)->get();
        }
        return view('admin.categories.posts.show', compact('post',"posts"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post=Post::find($id);
        return view('admin.categories.posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post=Post::find($id);
        $validation = Validator::make($request->all(), [
            'image' => 'required'
        ]);
        if(!$validation->fails()){
            $image1 = $request['img1'];  
            $virgula=strrpos($image1,',');
            $str=substr($image1, 0,$virgula+1);
            $bara=strrpos($image1,'image/');
            $punct=strrpos($image1,';base64');
            $extension= substr($str, $bara+6,$punct-$bara-6);
            $image1 = str_replace('data:image/'.$extension.';base64,', '', $image1);
            $image1 = str_replace(' ', '+', $image1);
    
            $imageName = 'post-'.$post->id.'-base.'.$extension;
           
            File::put('posts/images/'. $imageName, base64_decode($image1));
            $post->img=$imageName;
            $post->save();
        }

        $validation = Validator::make($request->all(), [
            'image_2' => 'required'
        ]);
        if(!$validation->fails()){

            $image2 = $request['img2'];  
            $virgula=strrpos($image2,',');
            $str=substr($image2, 0,$virgula+1);
            $bara=strrpos($image2,'image/');
            $punct=strrpos($image2,';base64');
            $extension= substr($str, $bara+6,$punct-$bara-6);
            $image2 = str_replace('data:image/'.$extension.';base64,', '', $image2);
            $image2 = str_replace(' ', '+', $image2);

            $imageName2 = 'post-'.$post->id.'.'.$extension;
            File::put('posts/images/'. $imageName2, base64_decode($image2));
            $post->img2=$imageName2;
            $post->save();
        }

        Post::where('id',$id)->update([
            'title' => $request['title'],
            'description' => $request['description'],
        ]);

        return route('post.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post=Post::find($id);
        Post::where('id',$id)->delete();
        return route('category.show', $post->cat_id);
    }

    public function setOnCategory($id){
        $post=Post::find($id);
        Post::where('cat_id',$post->cat_id)->update([
            'category_head'=>0
        ]);

        Post::where('id',$id)->update([
            'category_head'=>1
        ]);

        return 1;
    }
    
}   