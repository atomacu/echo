<?php

namespace App\Http\Controllers;

use App\Post;
use App\Category;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts3=null;
        $posts1=Post::where('slider_1_welcome',1)->get();
        $posts2=Post::where('aprove',1)->orderBy('updated_at', 'desc')->get();
        if(count($posts2)>=4){
            $posts2=Post::where('aprove',1)->orderBy('updated_at', 'desc')->take(4)->get();
        }
       
        $wCat=Category::where('on_welcome',1)->first();
        if($wCat){
            $posts3=Post::where('cat_id',$wCat->id)->where('aprove',1)->orderBy('updated_at', 'desc')->get();
            if(count($posts3)>=10){
                $posts3=Post::where('cat_id',$wCat->id)->where('aprove',1)->orderBy('updated_at', 'desc')->take(10)->get();
            }
        }
        
        return view('welcome',compact('posts1','posts2','posts3'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function adsTxt(){
        return response()->file('ads/ads.txt');
    }
}
