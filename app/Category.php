<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name','index'
    ];

    static function getLastIndex()
    {
        return self::orderBy('index', 'desc')->first()->index;
    }

    // public function lang(){
    //     return $this->belongsTo('App\Lang','lang_id');
    // }

    public function posts(){
        return $this->hasMany('App\Post',"cat_id")->where('aprove',1)->where('draft',0)->orderBy('updated_at', 'desc');
    }

    public function draft(){
        return $this->hasMany('App\Post',"cat_id")->where('draft',1)->orderBy('updated_at', 'desc');
    }

    public function userPosts(){
        return $this->hasMany('App\Post',"cat_id")->where('aprove',1)->where('draft',0)->where('category_head',0)->orderBy('updated_at', 'desc');
    }
    
    public function headPost(){
        return $this->hasOne('App\Post',"cat_id")->where('aprove',1)->where('draft',0)->where('category_head',1);
    }
}
