<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['role']], function(){
  
    Route::resource('welcome', 'WelcomeController');
    Auth::routes();
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('admin', 'Admin\AdminController');
    Route::resource('category', 'Admin\CategoryController');
    Route::put('category-name/{category}', 'Admin\CategoryController@updateName')->name('category.name.update'); 
    Route::resource('post', 'Admin\PostController')->except([
        'create'
    ]); 
    Route::get('/post/create/{id}', 'Admin\PostController@create')->name('post.create'); 
    Route::put('/post/setOnCategory/{id}', 'Admin\PostController@setOnCategory')->name('post.setOnCategory'); 
    
    Route::resource('ucategory', 'User\CategoryController');
    Route::resource('articles', 'User\PostController');

    
});


